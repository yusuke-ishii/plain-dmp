import * as types from '../mutation-types'
import Vue from 'vue'; // Vue.http に axios を設定済み。

export default {

  namespaced : true,

  state : {
      user: {},
      authenticated: false,
  },

  getters: {
    authenticated : state => {
      return state.authenticated;
    },
    user: state => {
      return state.user;
    }
  },

  mutations : {
      [types.LOGIN] (state, payload){
          state.user = payload.user;
          state.authenticated = payload.authenticated;
      },
      [types.LOGOUT] (state, payload){
          state.user = {};
          state.authenticated = false;
          localStorage.removeItem('jwt-token');
      }
  },


  actions : {

    login ({commit}, args ){ // email, password, successCb = null, errorCb = null) {
      let params = {
        "email": args.email,
        "password": args.password
      };
      Vue.http.post('authenticate', params).then( res => {
        let payload = {
          user: res.data.user,
          authenticated: true
        };
        commit(types.LOGIN, payload);
        args.successCb();
      }).catch( error => {
        args.errorCb();
      });
    },

    logout ({commit}){
      commit(types.LOGOUT);
    },

    init ({commit}, args){
      Vue.http.get('me').then(res => {
        let payload = {
          user: res.data.user,
          authenticated: true
        };
        commit(types.LOGIN, payload);
        args.successCb();
      }).catch(err => {
        if(err.response.data.error == 'token_invalid'){
          commit(types.LOGOUT);
        }
        args.errorCb();
      });
    }

  }

};
