import axios from 'axios';

export default (Vue) => {

  axios.defaults.baseURL = '/api';

  // Intercept the request to make sure the token is injected into the header.
  axios.interceptors.request.use(config => {
    config.headers['X-CSRF-TOKEN']     = window.Laravel.csrfToken;
    config.headers['X-Requested-With'] = 'XMLHttpRequest';
    config.headers['Authorization']    = `Bearer ${localStorage.getItem('jwt-token')}`;
    return config;
  });

  axios.interceptors.response.use(response => {
    // ...get the token from the header or response data if exists, and save it.
    const token = response.headers['Authorization'] || response.data['token'];
    if (token) {
      localStorage.setItem('jwt-token', token);
    }
    return response;
  }, error => {
    let errorMessage = error.response.data.error;
    if(errorMessage && errorMessage.match(/token_expired/) && location.pathname !== '/login'){
      location.href = '/login';
    }
    // Also, if we receive a Bad Request / Unauthorized error
    return Promise.reject(error);
  });

  Vue.http = axios;
  Object.defineProperties(Vue.prototype, {
    $http: {
      get () {
        return axios;
      }
    }
  });
};
