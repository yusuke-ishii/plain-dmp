<?php

Route::group(['middleware' => 'web', 'prefix' => 'dfpconnector', 'namespace' => 'Modules\DfpConnector\Http\Controllers'], function()
{
    Route::get('/', 'DfpConnectorController@index');
});
