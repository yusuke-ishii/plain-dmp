<?php

Route::group(['middleware' => 'web', 'prefix' => 'mediamanager', 'namespace' => 'Modules\MediaManager\Http\Controllers'], function()
{
    Route::get('/', 'MediaManagerController@index');
});
