<?php

Route::group(['middleware' => 'web', 'prefix' => 'tdconnector', 'namespace' => 'Modules\TdConnector\Http\Controllers'], function()
{
    Route::get('/', 'TdConnectorController@index');
});
