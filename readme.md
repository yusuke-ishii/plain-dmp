# LVF CoreUI Boilerplate

Laravel, Vue.js, PythonFire, CoreUI を統合したダッシュボードシステムの雛形です。

このリポジトリをクローンして、
```
$ composer install
$ yarn install
$ yarn run dev
$ php artisan serve
```
の順にコマンドを実行後、localhost:8000にアクセスすると、CoreUIの画面を表示できます。
CoreUIのソースは Laravel Mix でビルドできるように組み込まれています。

CoreUIのリンク
Github: https://github.com/mrholek/CoreUI-Vue
Demo: http://coreui.io/demo/Vue_Demo/#/dashboard


resources/fire以下の利用手順
```
cd resources
python -m venv fire ./fire
cd fire
source bin/activate
pip install -r requirements.txt
```

