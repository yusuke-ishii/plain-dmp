let mix = require('laravel-mix');
let path = require('path');

function resolve (dir) {
  return path.join(__dirname, '.', dir)
}

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');


// CoreUIの統合のため、aliasを追加。
mix.webpackConfig({
  resolve: {
    alias: {
      '@': resolve('resources/assets/js/')
    }
  }
});
