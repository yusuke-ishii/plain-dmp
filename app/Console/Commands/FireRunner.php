<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Libraries\Fire\PythonFireWrapper;

class FireRunner extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fire';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $fireApp = new PythonFireWrapper();
        // app.pyの関数呼び出しの例
        $res = $fireApp->hello('world');
        dd($res->data);
    }
}
